﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/LambertShader" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_SpecColor("Specular Color", Color) = (1,1,1,1)
		_Shininess("Shininess", float) = 10
	}
	SubShader {
		Pass{
			Tags { "LightMode" = "ForwardBase" }
		
			CGPROGRAM

			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction

			#include "UnityCG.cginc"

			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;

			uniform float4 _LightColor0;

			struct inputStruct{
				float4 vertexPos : POSITION;
				float3 vertexNormal : NORMAL;
			};

			struct outputStruct{
				float4 pixelPos: SV_POSITION;
				float4 pixelCol : COLOR;

				float3 normalDirection : TEXCOORD0;
				float4 pixelWorldPos : TEXCOORD1;
			};

			outputStruct vertexFunction(inputStruct input){
				outputStruct toReturn;

				float3 normalDirection = normalize(mul(float4(input.vertexNormal, 0.0), unity_ObjectToWorld).xyz);
				float3 viewDirection = normalize(float3(float4(_WorldSpaceCameraPos.xyz, 1.0) - mul(unity_ObjectToWorld, input.vertexPos).xyz));

//				float3 lightDirection;
//				float attenuation = 1.0;
//
//				lightDirection = normalize(_WorldSpaceLightPos0.xyz);
//
//				float3 diffuseReflection = attenuation * _LightColor0.xyz * max(0.0, dot(normalDirection, lightDirection));
//
//				float3 specularReflection = reflect(-lightDirection, normalDirection);
//
//				specularReflection = dot(specularReflection, viewDirection);
//				specularReflection = max(0.0, specularReflection);
//				//specularReflection = max(0.0, dot(normalDirection, lightDirection)) * specularReflection;
//
//				specularReflection = pow(max(0.0, specularReflection), _Shininess);
//				specularReflection = max(0.0, dot(normalDirection, lightDirection)) * specularReflection;
//
//				float3 finalLight = specularReflection + diffuseReflection + UNITY_LIGHTMODEL_AMBIENT;

				toReturn.normalDirection = normalDirection;
				toReturn.pixelWorldPos = float4(viewDirection.xyz, 1.0);

				toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);

				return toReturn;
			}	

			float4 fragmentFunction (outputStruct input) : COLOR
			{
				float3 lightDirection;
				float attenuation = 1.0;

				lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				float3 diffuseReflection = attenuation * _SpecColor.rgb * max(0.0, dot(input.normalDirection, lightDirection));

				float3 specularReflection = reflect(-lightDirection, input.normalDirection);

				specularReflection = dot(specularReflection, input.pixelWorldPos);
				specularReflection = max(0.0, specularReflection);
				//specularReflection = max(0.0, dot(normalDirection, lightDirection)) * specularReflection;

				specularReflection = pow(max(0.0, specularReflection), _Shininess);
				specularReflection = max(0.0, dot(input.normalDirection, lightDirection)) * specularReflection;

				float3 finalLight = specularReflection + diffuseReflection + UNITY_LIGHTMODEL_AMBIENT;

				input.pixelCol = float4(finalLight * _Color, 1.0);

				return input.pixelCol;
			}

			ENDCG
		}
	}
	//FallBack "Diffuse"
}
